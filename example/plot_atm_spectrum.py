"""
Atmosphere spectrum
===================

Plot spectrum of energy radiated by atmosphere if considered a black body at 25 [°C]
"""
from math import pi

import matplotlib.pyplot as plt
import numpy as np

from black_body.constants import kelvin
from black_body.energy import black_body, spectrum

temp_atm = kelvin(25)

wls = np.linspace(100, 50000, 1000)  # [nm]
ebs = [spectrum(wl * 1e-9, temp_atm) * 1e-9 * pi for wl in wls]

# plot result
fig, axes = plt.subplots(1, 1, figsize=(10, 6), squeeze=False)
ax = axes[0, 0]
ax.axhline(y=0, ls='--', color='#aaaaaa')
ax.plot(wls, ebs)
ax.text(0.8, 0.8, f"Total: {sum(ebs) * (wls[1] - wls[0]):.0f} [W.m-2]\n"
                  f"Stefan–Boltzmann: {black_body(temp_atm):.0f} [W.m-2]",
        ha='right', va='center', transform=ax.transAxes)
ax.set_xlabel("Wavelength [nm]")
ax.set_ylabel("[W.m-2.nm-1]")

fig.suptitle("Energy radiated by atmosphere")
fig.tight_layout()
plt.show()
