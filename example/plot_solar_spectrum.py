"""
Solar spectrum
==============

Plot solar spectrum and black body at same temperature
"""
from math import pi

import matplotlib.pyplot as plt
import pandas as pd

from black_body.constants import sun_earth_distance, sun_radius, sun_temp
from black_body.energy import black_body, spectrum

# read meas
df = pd.read_csv("data/sun_spectrum.csv", sep=";", comment="#", index_col=['wl'])
df = df.loc[:2500]

# computes black body
df['bb'] = [spectrum(wl * 1e-9, sun_temp) * 1e-9 * pi * sun_radius ** 2 / sun_earth_distance ** 2 for wl in df.index]

# plot result
fig, axes = plt.subplots(1, 1, figsize=(10, 6), squeeze=False)
ax = axes[0, 0]
ax.axhline(y=0, ls='--', color='#aaaaaa')
ax.plot(df.index, df['irr'], label="measure")
ax.plot(df.index, df['bb'], label="black body")
ax.text(0.8, 0.8, f"Total: {df['bb'].sum() * (df.index[1] - df.index[0]):.0f} [W.m-2]\n"
                  f"Stefan–Boltzmann: {black_body(sun_temp) * sun_radius ** 2 / sun_earth_distance ** 2:.0f} [W.m-2]",
        ha='right', va='center', transform=ax.transAxes)
ax.legend(loc='upper left')
ax.set_xlabel("Wavelength [nm]")
ax.set_ylabel("[W.m-2.nm-1]")

fig.suptitle("Energy radiated by sun received on top of atmosphere")
fig.tight_layout()
plt.show()
