Overview
========

.. {# pkglts, glabpkg_dev

.. image:: https://revesansparole.gitlab.io/black_body/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/black_body/1.1.0/


.. image:: https://revesansparole.gitlab.io/black_body/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/black_body


.. image:: https://revesansparole.gitlab.io/black_body/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/black_body/


.. image:: https://badge.fury.io/py/black_body.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/black_body




main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/black_body/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/black_body/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/black_body/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/black_body/commits/main
.. #}

Set of formalisms around black bodies
